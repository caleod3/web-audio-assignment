# Web-Audio Assignment 

'Contact Details' on about page is where the main issue is. When the site is brought down to close to a mobile display size the 'secondary' class items in the contact.html document extend to the outside of the remainder of the other elements.





___________________________________________________________________________________________________________________________________________________
                                                            INDEX.HTML
___________________________________________________________________________________________________________________________________________________

<!DOCTYPE html> <!-- DOCTYPE tells the browser this is a HTML document - it should always sit at the very top of the document -->
<html> <!-- Add HTML element -->
	<head> <!-- Container for HTML document meta information -->
	  <meta charset="utf-8"> <!-- The character set should be universal character set transformation format 8-bit -->
	  <title>CaleOD | MST</title> <!-- Title tab appears in the browser tabs and search engine results -->
	  <link rel="stylesheet" href="css/normalize.css"> <!-- Create link element to reference normalize.css file -->
	  <link rel="preconnect" href="https://fonts.gstatic.com">
	  <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@1,200&display=swap" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css2?family=Dela+Gothic+One&family=Overpass:ital,wght@1,200&display=swap" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@1,200&display=swap" rel="stylesheet">
	  <link rel="stylesheet" href="css/main.css"> <!-- Create link element to reference main.css file -->
	  <link rel="stylesheet" href="css/responsive.css"> 


	  <meta name="viewport" content="width=device-width, initial scale=1.0">
    </head> <!-- Create head element -  this will include metainformation (e.g. CSS) about the documentation - NOT THE SAME AS A HEADER -->
	
	<body> <!-- Create body element - this will include the body of the document i.e. the rest of the page information -->
		<header> <!-- Specifies a header for the document or section - used to contain introductory content or navigation -->
			<a href="index.html" id="logo"> <!-- Wrap h1 and h2 elements in an anchor <a> element - Home Button Navigation - apply id "logo" to anchor element that wraps the header --> 
			  <h1>Cale O' Donnell</h1> <!-- Note the persistent level of indentation to keep code readable -->
			  <h2>Music, Sound & Technology Student</h2>
			  <a href=https://www.ulster.ac.uk class="uniLink"> <!-- Creating an achor element to link h3 with ulster.ac.uk - Absolute URL path used here -->
				<p><span>Ulster University</span></p> 
			  </a>
			</a>
			<nav> <!-- Nav element - simply a statement indicating that the page navigation is here -->
				<ul id="navigation"> <!-- Unordered list element contains list of items typically styled with bullpoints -->
		            <!-- List element formats text in list form - <li> - should go inside either a <ul> or <ol> -->
					<li><a href="index.html" class="selected">Portfolio</a></li> <!-- Link to home page -->
					<li><a href="about.html">About</a></li> <!-- Link to about page --> 
					<li><a href="contact.html">Contact</a></li> <!-- Link to contact page --> 
				</ul>
			</nav>
		</header> <!-- Specifies a header for the document or section - used to contain introductory content or navigation links -->
		
		<div id="wrapper"> <!-- Create div element and give it an attribute called ID which we can in turn name "wrapper" -->
			<section> <!-- Make sure you keep indenting the code - Shortcut = select all and CMD + ] to indent by 1 tab (2 spaces) -->
				<ul id="gallery">
					<li> <!-- Creating something that can easily be copied and pasted across -->
						<a href="images/Headroom-GrannyFlat.jpg" target="_blank"> <!-- adding a link around the image which will allow users to click and view the full size image -->
							<img src="images/Headroom-GrannyFlat.jpg" alt="Band Rehearsal"> <!-- relative path - we're indicating where the file is relative to the index.html file -->  
							<p>Experiment with colour and texture</p> <!-- adding a caption for the image instead of using alt -->
						</a>
					</li> 
					<li> <!-- Creating something that can easily be copied and pasted across -->
						<a href="images/Headroom-FullBandSuited.jpg" target="_blank"> <!-- adding a link around the image which will allow users to click and view the full size image -->
							<img src="images/Headroom-FullBandSuited.jpg" alt="Band Suited Up"> <!-- relative path - we're indicating where the file is relative to the index.html file -->  
							<p>Experiment with colour and texture</p> <!-- adding a caption for the image instead of using alt -->
						</a>
					</li> 
					<li> <!-- Creating something that can easily be copied and pasted across -->
						<a href="images/AboutPageImage.jpg" target="_blank"> <!-- adding a link around the image which will allow users to click and view the full size image -->
							<img src="images/AboutPageImage.jpg" alt=""> <!-- relative path - we're indicating where the file is relative to the index.html file -->  
							<p>Experiment with colour and texture</p> <!-- adding a caption for the image instead of using alt -->
						</a>
					</li> 
					<li> <!-- Creating something that can easily be copied and pasted across -->
							<p>Experiment with colour and texture</p> <!-- adding a caption for the image instead of using alt -->
					</li> 
				</ul>
			</section> <!-- Section elements break up the information into logical groupings -->
			<footer> <!-- Complements the <header> - it represents the bottom of the content area -->
			  <a href="https://soundcloud.com/caleod-mst" target="_blank"><img src="images/soundcloud-logo.png" alt="SoundCloud Icon" class="social-icons"></a> <!-- Absolute URL path used here -->
			  <p>&copy; 2021 Cale O' Donnell</p> <!-- &copy; is a HTML entity for the copyright (©) symbol-->
			</footer> <!-- Close the footer element here -->
		</div> <!-- Close the div element here -->
	
	</body> <!-- Close the body element here -->
</html> <!-- Close the html element here -->

___________________________________________________________________________________________________________________________________________________
                                                            ABOUT.HTML
___________________________________________________________________________________________________________________________________________________

<!DOCTYPE html> <!-- DOCTYPE tells the browser this is a HTML document - it should always sit at the very top of the document -->
<html> <!-- Add HTML element -->
	<head> <!-- Container for HTML document meta information -->
	  <meta charset="utf-8"> <!-- The character set should be universal character set transformation format 8-bit -->
	  <title>CaleOD | MST</title> <!-- Title tab appears in the browser tabs and search engine results -->
	  <link rel="stylesheet" href="css/normalize.css"> <!-- Create link element to reference other files -->
	  <link rel="preconnect" href="https://fonts.gstatic.com">
	  <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@1,200&display=swap" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css2?family=Dela+Gothic+One&family=Overpass:ital,wght@1,200&display=swap" rel="stylesheet">
	  <link rel="stylesheet" href="css/main.css"> <!-- Create link element to reference other files -->
	  <link rel="stylesheet" href="css/responsive.css"> 



	  <meta name="viewport" content="width=device-width, initial scale=1.0">
    </head> <!-- Create head element -  this will include metainformation (e.g. CSS) about the documentation - NOT THE SAME AS A HEADER -->
	<body> <!-- Create body element - this will include the body of the document i.e. the rest of the page information -->
		<header> <!-- Specifies a header for the document or section - used to contain introductory content or navigation -->
			<a href="index.html" id="logo"> <!-- Wrap h1 and h2 elements in an anchor <a> element - Home Button Navigation - apply id "logo" to anchor element that wraps the header --> 
			  <h1>Cale O' Donnell</h1> <!-- Note the persistent level of indentation to keep code readable -->
			  <h2>Music, Sound & Technology Student</h2>
			  <a href=https://www.ulster.ac.uk class="uniLink"> <!-- Creating an achor element to link h3 with ulster.ac.uk - Absolute URL path used here -->
				<p><span>Ulster University</span></p> 
			  </a>
			</a>
			<nav> <!-- Nav element - simply a statement indicating that the page navigation is here -->
				<ul id="navigation"> <!-- Unordered list element contains list of items typically styled with bullpoints -->
		            <!-- List element formats text in list form - <li> - should go inside either a <ul> or <ol> -->
					<li><a href="index.html">Portfolio</a></li> <!-- Link to home page -->
					<li><a href="about.html" class="selected">About</a></li> <!-- Link to about page --> 
					<li><a href="contact.html">Contact</a></li> <!-- Link to contact page --> 
				</ul>
			</nav>
		</header> <!-- Specifies a header for the document or section - used to contain introductory content or navigation links -->
		
		<div id="wrapper"> <!-- Create div element and give it an attribute called ID which we can in turn name "wrapper" -->
			<section> <!-- Make sure you keep indenting the code - Shortcut = select all and CMD + ] to indent by 1 tab (2 spaces) -->
				<img src="images/AboutPageImage.jpg" alt="Photo Of Cale" class="profile-photo"/> 
				<h3>About</h3>
				<p>Hi, I'm Cale O' Donnell! This is my university portfolio where I will share all my projects and work.</p>
				<p>If you'd like to follow me on soundcloud, my username is <a href="https://soundcloud.com/caleod-mst">CaleOD-MST</a>.</p>
			</section> <!-- Section elements break up the information into logical groupings -->
			<footer class="paddingFooter"> <!-- Complements the <header> - it represents the bottom of the content area -->
			  <a href="https://soundcloud.com/caleod-mst"><img src="images/soundcloud-logo.png" alt="SoundCloud Icon" class="social-icons"></a> <!-- Absolute URL path used here -->
			  <p>&copy; 2021 Cale O' Donnell</p> <!-- &copy; is a HTML entity for the copyright (©) symbol-->
			</footer> <!-- Close the footer element here -->
		</div> <!-- Close the div element here -->
	
	</body> <!-- Close the body element here -->
</html> <!-- Close the html element here -->


___________________________________________________________________________________________________________________________________________________
                                                            CONTACT.HTML
___________________________________________________________________________________________________________________________________________________

<!DOCTYPE html> <!-- DOCTYPE tells the browser this is a HTML document - it should always sit at the very top of the document -->
<html> <!-- Add HTML element -->
	<head> <!-- Container for HTML document meta information -->
	  <meta charset="utf-8"> <!-- The character set should be universal character set transformation format 8-bit -->
	  <title>CaleOD | MST</title> <!-- Title tab appears in the browser tabs and search engine results -->
	  <link rel="stylesheet" href="css/normalize.css"> <!-- Create link element to reference other files -->
	  <link rel="preconnect" href="https://fonts.gstatic.com">
	  <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@1,200&display=swap" rel="stylesheet">
	  <link href="https://fonts.googleapis.com/css2?family=Dela+Gothic+One&family=Overpass:ital,wght@1,200&display=swap" rel="stylesheet">
	  <link rel="stylesheet" href="css/main.css"> <!-- Create link element to reference other files -->
	  <link rel="stylesheet" href="css/responsive.css"> 


	  <meta name="viewport" content="width=device-width, initial scale=1.0">
    </head> <!-- Create head element -  this will include metainformation (e.g. CSS) about the documentation - NOT THE SAME AS A HEADER -->
	<body> <!-- Create body element - this will include the body of the document i.e. the rest of the page information -->
		<header> <!-- Specifies a header for the document or section - used to contain introductory content or navigation -->
			<a href="index.html" id="logo"> <!-- Wrap h1 and h2 elements in an anchor <a> element - Home Button Navigation - apply id "logo" to anchor element that wraps the header --> 
			  <h1>Cale O' Donnell</h1> <!-- Note the persistent level of indentation to keep code readable -->
			  <h2>Music, Sound & Technology Student</h2>
			  <a href=https://www.ulster.ac.uk class="uniLink"> <!-- Creating an achor element to link h3 with ulster.ac.uk - Absolute URL path used here -->
				<p><span>Ulster University</span></p> 
			  </a>
			</a>
			<nav> <!-- Nav element - simply a statement indicating that the page navigation is here -->
				<ul id="navigation"> <!-- Unordered list element contains list of items typically styled with bullpoints -->
		            <!-- List element formats text in list form - <li> - should go inside either a <ul> or <ol> -->
					<li><a href="index.html">Portfolio</a></li> <!-- Link to home page -->
					<li><a href="about.html">About</a></li> <!-- Link to about page --> 
					<li><a href="contact.html" class="selected">Contact</a></li> <!-- Link to contact page --> 
				</ul>
			</nav>
		</header> <!-- Specifies a header for the document or section - used to contain introductory content or navigation links -->
		
		<div class="contactPage" id="wrapper"> <!-- Create div element and give it an attribute called ID which we can in turn name "wrapper" -->
			<section id="primary"> <!-- Make sure you keep indenting the code - Shortcut = select all and CMD + ] to indent by 1 tab (2 spaces) -->
				<h3>General Information</h3>
				<p>I'm currently looking to do collaborative work in both the development of applications and music production. Please don't hesitate to get in touch with any questions you might have</p>
				<!-- Adding another <p> element to create some seperation -->
				<p>Phone contact may go to answering service - I will respond as soon as I can. Otherwise, Instagram and email are the best ways to reach me. Thanks!!</p>
			</section> <!-- Section elements break up the information into logical groupings -->
			<section id="secondary">
				<h3>Contact Details</h3>
				<ul class="contact-info">
					<li class="phone"><a href="tel: +353 12 345 6789">+353 (0)12 345 6789</a></li>
					<li class="Instagram"><a href="https://www.instagram.com/caleod_mst/">Instagram</a></li>
					<li class="mail"><a href="mailto: dud.abc@ulster.ac.uk">dud.abc@ulster.ac.uk</a></li>
				</ul>
			</section>
			<footer class="paddingFooter"> <!-- Complements the <header> - it represents the bottom of the content area -->
			  <a href="https://soundcloud.com/caleod-mst"><img src="images/soundcloud-logo.png" alt="SoundCloud Icon" class="social-icons"></a> <!-- Absolute URL path used here -->
			  <p>&copy; 2021 Cale O' Donnell</p> <!-- &copy; is a HTML entity for the copyright (©) symbol-->
			</footer> <!-- Close the footer element here -->
		</div> <!-- Close the div element here -->
	
	</body> <!-- Close the body element here -->
</html> <!-- Close the html element here -->



___________________________________________________________________________________________________________________________________________________
                                                            MAIN.CSS
___________________________________________________________________________________________________________________________________________________

/**********************************************
                     GENERAL
***********************************************/
a {
	text-decoration: none; /**** Remove all underline on anchor elements ****/
}

body {
 	font-family: font-family: 'Overpass', sans-serif; 
 }

#wrapper { /**** This now selects the div we just created ****/
/*	max-width: 128em;
*/	max-width: 100%;
	margin: 0 auto;
	padding: 0 5%;
	text-align: center;
}

#wrapper p {
	font-size: 1.3em;
}

img {
	max-width: 100%;
}

h3 {
	font-size: 2.5em;
	margin: 0 0 1em 0;
}

#primary {
	font-family: 'Poppins', sans-serif; 
}

/*#secondary {
}
*/
/***********************
        HEADING
***********************/
#logo {
	font-family: 'Dela Gothic One', sans-serif;
/*	font-family: 'Overpass', sans-serif; 
*/	text-align: center; 
	margin: -5% 0 0;
	font-size: 1.2em;
}

h1, h2 {
	margin: -0.1em 0 -0.2em;
	padding: 0.4em 0 0em;
	font-size: 1.4em;
	font-weight: normal;
	line-height: 0.8em;
	letter-spacing: 0.05em;
}

h2 {
	line-height: 1em;
}

.uniLink {
	font-weight: bold;
	font-family: 'Overpass', sans-serif; 
/*	font-family: 'Dela Gothic One', cursive;
*/	font-size: 1.4em;
	text-align: center;
}


/***********************
       NAVIGATION     
***********************/
#navigation {
	list-style-type: none;
	display: flex;
	justify-content: space-between;
	align-items: center;
	font-weight: lighter;
	font-family: 'Dela Gothic One', cursive;
	padding: 0.5em 2%;
}

nav {
	text-align: center;
	padding: 0.017px 0;
	margin: -5px 0 0;
}



section a { /**** Select all of the anchor elements ****/
	font-family: 'Overpass', sans-serif;

}

header {
	float: left;
	margin: 0 0 10px 0;
	width: 100%;
}


/***********************
      PORTFOLIO PAGE
***********************/
#gallery {
	margin: 0;
	padding: 0;
	list-style-type: none;
}

#gallery li {
	float: left;
	width: 45%;
	margin: 2.5%;
	color: red;
}

#gallery li p {
	margin: 0;
	padding: 5%;
	font-size: 0.75em;
	font-family: 'Overpass', sans-serif;
	color: #dfe6e4;
	background-color: #2D6A4F;
}


/***********************
       ABOUT PAGE
***********************/

.profile-photo {
	display: block;
	max-width: 200px;
	margin: 0 auto 30px;
	border-radius: 100%;
}




/***********************
       CONTACT PAGE
***********************/

.contact-info {
	background-color: #2D6A4F;
	list-style: none;
	padding: 0;
	margin: 0;
	font-size: 2em;
}

.contact-info a {
	display: block;
	min-height: 20px;
	background-repeat: no-repeat;
	background-size: 40px 40px;
	padding: 0 0 0 40px;
	margin: 0 0 25px;
}

.contact-info li.phone a {
	background-image: url('../images/noun_Phone.svg');
}

.contact-info li.Instagram a {
	background-image: url('../images/noun_Insta.svg');
}

.contact-info li.mail a {
	background-image: url('../images/noun_Mail.svg');
}

.contactPage li {
/*	background-color: #95D5B2;
*/	display: inline-block;


}

.contactPage {
	margin-left: 1em;
}

.contactPage p {
	font-size: 1.3em;
}


/***********************
        FOOTER
***********************/

footer {
	padding-top: 50px;
	font-size: 0.6em;
	text-align: center;
	list-style: none;
	clear: both;
}

footer p {
/*	font-family: 'Dela Gothic One', cursive;
*/	font-family: 'Overpass', sans-serif;
	font-weight: bolder;
}

/* USE FOR RESIZING ICONS */
.social-icons {
	width: 60px;
	height: 60px;
	margin: 0 5px;
}


.paddingFooter {
	padding-top: 13vh;
}




/***********************
        COLORS
***********************/

a {
	color: #FF7700; /* Soundcloud Orange */
}

header {
	background-color: #081C15;

/*	
*/}

h1, h2 {
	color: #B7E4C7;
}

h3 {
	font-family: 'Overpass', sans-serif;
	color: #dfe6e4;
}

.uniLink {
	color: #dfe6e4;
}

span:hover {
	color: #041e42; /* Blue color matched from UU Website */
}

nav {
	background: #B7E4C7;
}

nav a, nav:visited {
	color: #52B788;
}

nav a.selected {
	color: #081C15;
}

/* I seperated these two from the same line to have the hover act as a different */

nav a:hover {
	color: #2D6A4F;
}

body {
	background: #2D6A4F;
	color: #52B788;
}

footer {
	background: #2D6A4F;
/*	background: #081C15;
*/	color: #dfe6e4;
}

#gallery li {
	background-color: #52B788;
}







/*****************************
            NOTES
*****************************/

/**************************
MY OWN H1, H2 Styling Code
***************************/

/*h1, h2 {
	margin: -0.1em auto -0.2em;
	padding: 0.4em 0 0em;
	font-size: 2em;
}
*/





/***********************
MONOCHROME GREEN COLORS
***********************/

/* ORDERED FROM BRIGHTEST TO DARKEST */
/*
#B7E4C7
#95D5B2
#52B788
#2D6A4F (FOOTER + NAVIGATION)
#081C15
*/ 

___________________________________________________________________________________________________________________________________________________
                                                            RESPONSIVE.CSS
___________________________________________________________________________________________________________________________________________________

@media screen and (min-width: 480px) { /* Using @media CSS rule - Target screens with a minimum width of 480px - if both are true, the CSS is added to the page */

/*************************
    TWO COLUMN LAYOUT
*************************/
	#primary { /* Using an ID to identify our main column i.e. the larger of the two columns in our design */
		width: 50%; /* Width set to 50% as to take up half of the screen */
		float: left;
	}
	#secondary { /* Using an ID to identify our second column */
 		width: 40%; /* Width set to 40% as to take up just less than half of the screen */
	 	float: right; /* Floating this column right to avoid unexpected browser issues - float: left; should work as long as the ID's are in order in the HTML */
	}

}

@media screen and (min-width: 660px) {  /* Second media query break point */

}

/*************************
   COMMON MQ BREAKPOINTS
*************************/
/*
320px — 480px: Mobile devices
481px — 768px: iPads, Tablets
769px — 1024px: Small screens, laptops
1025px — 1200px: Desktops, large screens
1201px and more —  Extra large screens, TV
*/

___________________________________________________________________________________________________________________________________________________
                                                            NOTES
___________________________________________________________________________________________________________________________________________________

TYPOGRAHPY STILL TO BE SORTED, ANY FONT SUGGESTIONS ARE WELCOME AND ANYTHING ELSE YOU THINK I MIGHT BE ABLE TO GET THE JIST OF FROM WHAT YOU SEE HERE. NOT ALLOWED TO USE ANY JS FOR THE APPLICATION. CHEERS MAN. 
